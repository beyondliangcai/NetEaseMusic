src.widgets package
===================

Submodules
----------

src.widgets.login_dialog module
-------------------------------

.. automodule:: src.widgets.login_dialog
    :members:
    :undoc-members:
    :show-inheritance:

src.widgets.music_table_widget module
-------------------------------------

.. automodule:: src.widgets.music_table_widget
    :members:
    :undoc-members:
    :show-inheritance:

src.widgets.playlist_widget module
----------------------------------

.. automodule:: src.widgets.playlist_widget
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.widgets
    :members:
    :undoc-members:
    :show-inheritance:
